﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GH2017.OnlineRound {
   public class Video {
      public int Size { get; }
      public int Id { get; }

      public Video(int id, int size) {
         Id = id;
         Size = size;
      }

      public override string ToString() {
         return $"Video {Id} size: {Size}MB";
      }
   }
}
