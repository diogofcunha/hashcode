﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GH2017.OnlineRound {
   public class Request {
      public int VideoId { get; }
      public int EndpointId { get; }
      public int Count { get; }
      public int Id { get; }

      public long Score { get; set; }

      public Request(int id, int videoId, int endpointId, int nRequests) {
         Id = id;
         VideoId = videoId;
         EndpointId = endpointId;
         Count = nRequests;
      }

      public override string ToString() {
         return $"{Count} requests for video {VideoId} from endpoint {EndpointId}";
      }
   }
}
