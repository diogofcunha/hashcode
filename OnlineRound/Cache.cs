﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GH2017.OnlineRound {
   public class Cache {
      public int Size { get; }
      public int Id { get; }

      private int _usedSize { get; set; } = 0;

      private Dictionary<int, Video> _cachedVideos;

      public Cache(int id, int size) {
         Id = id;
         Size = size;
         _usedSize = 0;
         _cachedVideos = new Dictionary<int, Video>();
      }

      public bool UsedCache() {
         return _cachedVideos.Count > 0;
      }

      public string GetOutput() {
         return String.Join(" ", new int[] { Id }.Concat(_cachedVideos.Keys));
      }

      public bool HasVideo(int videoId) {
         return _cachedVideos.ContainsKey(videoId);
      }

      public bool AddVideo(Video v) {
         if (_cachedVideos.ContainsKey(v.Id)) {
            return true;
         }
     
         if (_usedSize + v.Size <= Size) {
            _usedSize += v.Size;
            _cachedVideos.Add(v.Id, v);
            return true;
         }

         return false;
      }

      public override string ToString() {
         return $"Cache {Id} size: {Size}MB";
      }
   }
}
