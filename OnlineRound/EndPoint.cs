﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GH2017.OnlineRound {
   public class EndPoint {

      public class EndPointToCacheLatency {
         public int CacheId { get; set; }
         public int EndPointId { get; set; }
         public int Latency { get; set; }

         public EndPointToCacheLatency(int c, int e, int l) {
            CacheId = c;
            EndPointId = e;
            Latency = l;
         }

         public override string ToString() {
            return $" Cache {CacheId} has {Latency} latency";
         }
      }

      public int Latency { get; }
      public int Id { get; }

      private int[] LatencyToCache { get; }

      public long Score { get; set; }

      public EndPointToCacheLatency[] LatencyToCacheObjs { get; set; }

      public EndPoint(int id, int latency, int nCache) {
         Id = id;
         Latency = latency;
         LatencyToCache = Enumerable.Repeat(-1, nCache).ToArray();
      }

      public void AddCacheLatency(int index, int latency) {
         LatencyToCache[index] = latency;
      }

      public void BuildList() {
         var l = new List<EndPointToCacheLatency>();
         for (var i = 0; i < LatencyToCache.Length; i++) {
            if (LatencyToCache[i] > -1) {
               l.Add(new EndPointToCacheLatency(i, Id, LatencyToCache[i]));
            }
         }
         LatencyToCacheObjs = l.OrderBy(c => c.Latency).ToArray();
      }

      public override string ToString() {
         var txt = String.Empty;
         for(var i = 0; i < LatencyToCache.Length; i++) {
            txt += (LatencyToCache[i] > -1 ?
               $" Cache {i} has {LatencyToCache[i]} latency" :
               $" Cache {i} no connection") + Environment.NewLine;
         }
         return String.Join(Environment.NewLine, new string[] { $"Endpoint {Id} has {Latency} to data center and connects to {LatencyToCacheObjs.Length} caches" }.Union(LatencyToCacheObjs.Select(l => l.ToString())));
      }
   }
}
