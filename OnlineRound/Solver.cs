﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GH2017.OnlineRound {
   internal class Solver : ISolver {

      public List<Cache> Caches { get; set; }
      public List<Video> Videos { get; set; }
      public List<EndPoint> EndPoints { get; set; }
      public List<Request> Requests { get; set; }

      public IEnumerable<string> Solve(IEnumerable<string> inputTextLines, bool verbose) {
         _readLineCount = 0;
         var textInput = inputTextLines.ToArray();
         Func<int[]> getLine = () => ReadNextLine(textInput);
         var inputs = getLine();

         var nVideos = inputs[0];
         var nEndPoints = inputs[1];
         var nRequests = inputs[2];
         var nCache = inputs[3];
         var cacheSize = inputs[4];

         // Caches
         Caches = new List<Cache>();
         for(var i = 0; i < nCache; i++) {
            Caches.Add(new Cache(i, cacheSize));
         }

         // Videos
         Videos = new List<Video>();
         inputs = getLine();
         for (var i = 0; i < inputs.Length; i++) {
            Videos.Add(new OnlineRound.Video(i, inputs[i]));
         }

         // EndPoints
         EndPoints = new List<EndPoint>();
         for(var i = 0; i < nEndPoints; i++) {
            inputs = getLine();
            var ep = new EndPoint(i, inputs[0], nCache);
            EndPoints.Add(ep);

            for(var j = 0; j < inputs[1]; j++) {
               var cL = getLine();
               ep.AddCacheLatency(cL[0], cL[1]);
            }
            ep.BuildList();
         }

         // Requests
         Requests = new List<Request>();
         for(var i = 0; i < nRequests; i++) {
            inputs = getLine();
            Requests.Add(new Request(i, inputs[0], inputs[1], inputs[2]));
         }

         // ALGORITHM --
         DelegateRequests();
         var score = CalculateScore();
         _totalScore += score;
         Console.WriteLine($"++ Score: {score} ++");
         Console.WriteLine($"++ Total Score: {_totalScore} ++");

         // DEBUG
         if (verbose) {
            Console.WriteLine("Caches");
            Caches.ForEach(c => Console.WriteLine(c.ToString()));
            Console.WriteLine("Videos");
            Videos.ForEach(c => Console.WriteLine(c.ToString()));
            Console.WriteLine("EndPoints");
            EndPoints.ForEach(c => Console.WriteLine(c.ToString()));
            Console.WriteLine("Requests");
            Requests.ForEach(c => Console.WriteLine(c.ToString()));
         }

         // OUTPUT
         var usedCaches = Caches.Where(c => c.UsedCache());
         var sb = new List<string>();
         sb.Add(usedCaches.Count().ToString());
         foreach(var c in usedCaches) {
            sb.Add(c.GetOutput());
         }

         return sb;
      }

      private int CalculateScore() {
         long totalTime = 0;
         long totalHits = 0;
         foreach(var ep in EndPoints) {
            var req = Requests.Where(r => r.EndpointId == ep.Id);
            foreach(var r in req) {
               totalHits += r.Count;

               foreach(var c in ep.LatencyToCacheObjs) {
                  if (Caches[c.CacheId].HasVideo(r.VideoId)) {
                     totalTime += r.Count * (ep.Latency - c.Latency);
                     break;
                  }
               }
            }
         }

         return (int)(((double)totalTime / totalHits) * 1000);
      }

	private IOrderedEnumerable<Request> GetRequestOrder() {
         // By the Request count
        //return Requests.OrderByDescending(x => x.Count);

         return Requests.OrderByDescending(x => {

            var y = Videos[x.VideoId].Size;
            return x.Count / y;
         });
      }

      private void DelegateRequests() {
         var orderedRequests = GetRequestOrder();
         
         foreach (var req in orderedRequests) {
            var endPoint = EndPoints[req.EndpointId];
            var video = Videos[req.VideoId];

            CacheVideo(video, endPoint);
         }
      }

      private void CacheVideo(Video v, EndPoint ep) {
         foreach (var c in ep.LatencyToCacheObjs) {
            var cacheId = c.CacheId;
            if (Caches[cacheId].AddVideo(v)) {
               break;
            }
         }
      }

      private int _readLineCount = 0;
      private long _totalScore = 0;
      private int[] ReadNextLine(string[] inputTextLines) {
         return inputTextLines[_readLineCount++].Split(' ').Select(c => Int32.Parse(c)).ToArray();
      }
   }
}
