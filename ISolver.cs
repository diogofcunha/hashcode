﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GH2017 {
   interface ISolver {
      IEnumerable<string> Solve(IEnumerable<string> inputTextLines, bool verbose);
   }
}
