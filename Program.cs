﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GH2017 {
   public class Program {
      private const string INPUT_FOLDER = @".\..\..\input\";
      private const string OUTPUT_FOLDER = @".\..\..\output\";
      private static ISolver _solver = new OnlineRound.Solver();
      static void Main(string[] args) {
         // UI for problem solving
         var inputFiles = Directory.EnumerateFiles(INPUT_FOLDER, @"*.in").ToArray();
         while (true) {

            // Available files
            Console.WriteLine("Please select file to process...");
            for(var idx = 0; idx < inputFiles.Length; idx++) {
               Console.WriteLine($"[{idx}] {Path.GetFileName(inputFiles[idx])}");
            }
            Console.WriteLine("[a] all (except example)");
            Console.WriteLine();
            Console.WriteLine($"[q] quit");
            Console.WriteLine();

            // Read action
            var action = Console.ReadKey().KeyChar.ToString();
            Console.Clear();
            var fileIndex = 0;
            if (Int32.TryParse(action, out fileIndex) && fileIndex >= 0 && fileIndex < inputFiles.Length) {
               Console.WriteLine($"Ok reading {inputFiles[fileIndex]}...");
               var txt = _solver.Solve(File.ReadAllLines(inputFiles[fileIndex]), false);
               WriteToFile(txt, inputFiles[fileIndex]);
            } else if (action.ToLower()  == "a") {
               foreach(var f in inputFiles) {
                  if (f.Contains("example")) continue;
                  Console.WriteLine($"Ok reading {f}...");
                  var txt = _solver.Solve(File.ReadAllLines(f), false);
                  WriteToFile(txt, f);
               }
            }
            else if (action.ToLower() == "q") {
               break;
            } else {
               Console.WriteLine($"Sorry I don't know what \"{action}\" is...");
            }
            
            Console.WriteLine();
         }
      }

      static void WriteToFile(IEnumerable<string> txt, string inputFile) {
         if (txt == null || txt.Count() == 0) return;

         var newFileName = $"{Path.GetFileNameWithoutExtension(inputFile)}.out";
         using (StreamWriter outputFile = new StreamWriter(OUTPUT_FOLDER + newFileName, false)) {
            Console.WriteLine($"Writing to file {OUTPUT_FOLDER + newFileName}");
            foreach (string line in txt) {
               outputFile.WriteLine(line);
            }
         }
      }
   }
}
