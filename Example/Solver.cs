﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GH2017.Example {
   internal class Solver : ISolver {

      public IEnumerable<string> Solve(IEnumerable<string> inputTextLines, bool verbose) {
         var rules = inputTextLines.First().Split(' ').Select(c => Int32.Parse(c)).ToArray();

         var pizza = new Pizza(rules[0], rules[1]);
         foreach (var l in inputTextLines.Skip(1)) pizza.TopPizza(l);
         return new string[] { inputTextLines.First(), pizza.ToString() };         
      }
   }
}
