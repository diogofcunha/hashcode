﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GH2017.Example {
   internal class PizzaSlicer {

      internal class Slice {
         internal int R1 { get; set; }
         internal int R2 { get; set; }
         internal int C1 { get; set; }
         internal int C2 { get; set; }

         internal Slice (int r1, int c1, int r2, int c2) {
            R1 = r1;
            R2 = r2;
            C1 = c1;
            C2 = c2;
         }

         public override string ToString() {
            return $"{R1} {C1} {R2} {C2}";
         }
      }

      internal int MaxBlocksInSlice { get; }
      internal int MinIngredientsInSlice { get; }

      internal List<Slice> Slices { get; }
      internal Slice[,] SliceMap { get; }
      internal Pizza Pizza { get;  }

      internal PizzaSlicer(Pizza p, int maxB, int minI) {
         MaxBlocksInSlice = maxB;
         MinIngredientsInSlice = minI;
         Slices = new List<Slice>();
         SliceMap = new Slice[p.Rows, p.Columns];
         Pizza = p;
      }

      internal void CutSlices() {
         Slices.Clear();
         
         for(int r = 0; r < Pizza.Rows; r++) {
            for(int c = 0; c < Pizza.Columns; c++) {
               // Already part of a slice
               if (SliceMap[r, c] != null) continue;

               var newSlice = new Slice(r, c, r, c);
            }
         }

         Console.WriteLine(Slices.Count);
         Slices.ForEach(s => Console.WriteLine(s));
      }

      private int RemainingSliceSpace(Slice s) {
         return MaxBlocksInSlice - (Math.Abs(s.R1 - s.R2) + 1) * (Math.Abs(s.C1 - s.C2) + 1);
      }
   }
}
