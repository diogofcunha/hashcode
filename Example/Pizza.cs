﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GH2017.Example {
   internal class Pizza {
      internal enum Ingredient {
         Tomato,
         Mushroom
      }

      internal int Columns { get; }
      internal int Rows { get; }

      internal Ingredient[,] PizzaMap { get; }

      internal Pizza(int rows, int columns) {
         Columns = columns;
         Rows = rows;
         PizzaMap = new Ingredient[rows,columns];
      }

      private int _nextRowToTop = 0;
      internal void TopPizza(string ingredients) {
         for(var idx = 0; idx < ingredients.Length; idx++) {
            PizzaMap[_nextRowToTop, idx] = ingredients[idx] == 'T' ? Ingredient.Tomato : Ingredient.Mushroom;
         }
         _nextRowToTop++;
      }

      private char GetIngredientChar(Ingredient i) {
         switch (i) {
            case Ingredient.Tomato:
               return 'T';
            case Ingredient.Mushroom:
               return 'M';
            default:
               throw new Exception("unknown ingredient");
         }
      }

      internal Ingredient GetPizzaBlock(int column, int row) {
         return PizzaMap[row, column];
      }

      public override string ToString() {
         var s = new StringBuilder();
         for (var l = 0; l < Rows; l++) {
            for(var c = 0; c < Columns; c++) {
               s.Append(GetIngredientChar(GetPizzaBlock(c, l)));
            }
            s.AppendLine();
         }
         return s.ToString();
      }
   }
}
